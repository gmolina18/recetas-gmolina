package com.example.recetas_gerardo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private EditText et_nomreceta;
    private TextView txt_Ingredientes, txt_InfoNutricional, textnomreceta;
    private Button bt_buscarReceta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txt_Ingredientes = (TextView) findViewById(R.id.txt_Ingredientes);
        this.textnomreceta = (TextView) findViewById(R.id.textnomreceta);
        this.txt_InfoNutricional = (TextView) findViewById(R.id.txt_InfoNutricional);
        this.et_nomreceta = (EditText) findViewById(R.id.et_nomreceta);
        this.bt_buscarReceta = (Button) findViewById(R.id.bt_buscarReseta);

        bt_buscarReceta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final String nombre = et_nomreceta.getText().toString().trim();

                String url = "http://api.edamam.com/search?q="+nombre+"&app_id=396a552c&app_key=76fd5386913bf45dbf3dead2b45cd200";

                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Respuesta
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);

                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    String NombreReceta = recipesJSON.getString("label");
                                    textnomreceta.setText(NombreReceta);
                                    JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                                    String ingre = "" ;

                                    for(int i = 0; i<in.length();i++){

                                        ingre = ingre + in.getString(i)+ "\n" ;


                                    }
                                    txt_Ingredientes.setText(ingre);


                                    JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                    JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                                    String enerlabel = enerJSON.getString("label");
                                    String enerq = enerJSON.getString("quantity");
                                    String eneru = enerJSON.getString("unit");

                                    JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                                    String fatlabel =fatJSON.getString("label");
                                    String fatq = fatJSON.getString("quantity");
                                    String fatu = fatJSON.getString("unit");

                                    JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                                    String fasatlabel = fasatJSON.getString("label");
                                    String fasatq = fasatJSON.getString("quantity");
                                    String fasatu = fasatJSON.getString("unit");


                                    String todo = enerlabel + " = " + enerq + " - " + eneru + "\n" +
                                            fatlabel + " = " + fatq + " - " + fatu + "\n" +
                                            fasatlabel + " = " + fasatq + " - " + fasatu + "\n" ;

                                    txt_InfoNutricional.setText(todo);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });

    }

}